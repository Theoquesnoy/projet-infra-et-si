# Suivi de projet Infra&SI

## Documentation par QUESNOY Théo et FARANT Yann

> ATTENTION : Les VM utilisées doivent êtres toute sur la même carte réseau virtuelle.

- Ajouter une carte réseau virtuelle

- Sur toutes les VMs, il faudra se rajouter dans le groupe `sudo`. Pour cela :

  - Passer en super utilisateur `su -`

  - Installer le paquet `sudo` s'il ne l'est pas

  - Exécuter cette commande `sudo usermod -aG sudo <username>`

  - Redémarrer la machine. Vous pourrez alors installer des paquets avec `sudo`

- Installer le paquet `vim`

Sur une première VM :

- Créer et configurer un serveur DHCP :

  - Configurer l'ip de la machine et en mettre une statique

  - Relancer sa configuration en faisant `sudo ifup <interface>` et vérifier avec `ip a`

  - Installer le paquet `isc-dhcp-server` avec les commandes `apt-get update`, `apt-get upgrade`, `apt-get install -y isc-dhcp-server`

  - Editer le fichier `sudo vim /etc/default/isc-dhcp-server`, Commentez la ligne `INTERFACESv6` si elle ne l'ai pas puis au niveau de la ligne `INTERFACESv4=`, ajouter l'interface où vous avez défini votre adresse IP statique

  - Editer ensuite le fichier `sudo vim /etc/dhcp/dhcpd.conf` et ajouter votre subnet range, c'est-à-dire les adresses IP que le serveur va fournir et d'autres informations.

    Vous devriez avoir une configuration comme celle-ci :

    ![](https://i.imgur.com/XIQ105q.png)

    Où vous renseignez votre réseau, votre masque ainsi que la range des adresse IP.

    > Toutes les autres configurations de subnet doivent être commenté

  - Ajouter le lancement du serveur automatiquement lors du démaragge de la VM avec la commande `sudo systemctl enable isc-dhcp-server.service`

  - Puis le lancer avec `sudo systemctl start isc-dhcp-server.service`

  - Vérifier qu'il est bien activé avec `systemctl status isc-dhcp-server.service`

Sur une deuxième VM :

- Configurer l'adresse IP du serveur :

  - Configurer l'interface en question et la mettre une adresse IP statique

  - Relancer sa configuration en faisant `sudo ifup <interface>`

- Installer le serveur apache :

  - Installer le serveur apache avec les commandes `apt-get update`, `apt-get upgrade`, `apt-get install -y apache2`

  - Ajouter le lancement du service automatiquement lors du démaragge de la VM avec la commande `sudo systemctl enable apache2.service`

  - Puis le lancer avec `sudo systemctl start apache2.service`

  - Vérifier qu'il est bien activé avec `systemctl status apache2.service`

- Configurer PortSentry :

  - Installer le paquet `portsentry` avec les commandes `apt-get install -y portsentry`

  - Vérifier si le paquet `iptables` est installé sinon l'installer

  - Configurer le fichier `sudo vim /etc/default/portsentry` puis mettre `TCP_MODE` et `UDP_MODE` en `atcp`

    ![](https://i.imgur.com/K0W7hRA.png)

  - Configurer ensuite le fichier de configuration principal `sudo vim /etc/portsentry/portsentry.conf`où au niveau des lignes `BLOCK_UDP` et `BLOCK_TCP` on les mets égal à 1.

    ![](https://i.imgur.com/D1hc1Bb.png)

  - Dans ce même fichier, il faudra commenter toutes les lignes commençant par `KILL_ROUTE` excepté celle-ci : `KILL_ROUTE="/sbin/iptables -I INPUT -s $TARGET$ -j DROP"`. Sorter du fichier et vérifier que c'est bien le cas avec la commande `cat portsentry.conf | grep KILL_ROUTE | grep -v "#"`

  - Ajouter le lancement du service automatiquement lors du démaragge de la VM avec la commande `systemctl enable portsentry.service`

  - Puis le lancer avec `systemctl start portsentry.service`

  - Vérifier qu'il est bien activé avec `systemctl status portsentry.service`

- Configurer fail2ban et les alertes discord :

  - Installer le paquet `fail2ban` avec les commandes `apt-get install -y fail2ban`

  - Copier le fichier de configuration pour pourvoir faire une configuration locale `sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local`

  - Une fois copié, éditer le fichier `jail.local` et ajouter au niveau de la jail `[sshd]` ceci :

    ```
    maxentry = 2
    bantime = 60
    action = discord_notifications[discord_userid=<@!XXXXXXXXXXXXXXXXXX>]
             %(action_)s
    ```

  - Pour récupérer votre userid de discord, mettre discord en mode développeur dans les paramètres avancés, puis clic droit sur votre avatar et copier l'identifiant

  - Ajouter également dans les jails `[dropbear]` et `[selinux-ssh]` la ligne :

    ```
    action = discord_notifications[discord_userid=<@!XXXXXXXXXXXXXXXXXX>]
             %(action*)s
    ```

  - Les jails devraient ressembler à ça :

    ![](https://i.imgur.com/Db6mMLe.png)

  - Pour pouvoir paramétrer des alertes discord, il faut créer un webhook, pour cela :

    - Créer un serveur

    - Aller dans les paramètres du serveur et aller sur l'onglet `Intégrations`

    - Webhook puis `Nouveau webhook`

    - Récupérer l'URL du webhook

  - Une fois le webhook créé, créer un fichier `sudo vim /etc/fail2ban/action.d/discord_notifications.conf` puis rajouter ces lignes :

    ```
    [Definition]

    # Notify on Startup
    actionstart = curl -X POST "<webhook>" \
                -H "Content-Type: application/json" \
                -d '{"username": "Fail2Ban", "content":":white_check_mark: The **[<name>]** jail has started"}'

    # Notify on Shutdown
    actionstop = curl -X POST "<webhook>" \
                -H "Content-Type: application/json" \
                -d '{"username": "Fail2Ban", "content":":no_entry: The **[<name>]** jail has been stopped"}'

    #
    actioncheck =

    # Notify on Banned
    actionban = curl -X POST "<webhook>" \
                -H "Content-Type: application/json" \
                -d '{"username":"Fail2Ban", "content":":bell: Hey <discord_userid>! **[<name>]** :hammer:**BANNED**:hammer: IP: `<ip>` for <bantime> hours after **<failures>** failure(s). Here is some info about the IP: https://db-ip.com/<ip>"}'
                curl -X POST "<webhook>" \
                -H "Content-Type: application/json" \
                -d '{"username":"Fail2Ban", "content":"If you want to unban the IP run: `fail2ban-client unban <ip>`"}'

    # Notify on Unbanned
    actionunban = curl -X POST "<webhook>" \
                -H "Content-Type: application/json" \
                -d '{"username":"Fail2Ban", "content":":bell: **[<name>]** **UNBANNED** IP: [<ip>](https://db-ip.com/<ip>)"}'
    [Init]

    # Name of the jail in your jail.local file. default = [your-jail-name]

    default = [sshd]

    # Discord Webhook URL
    webhook = https://discordapp.com/api/webhooks/XXXXXXXXXXXXXXX/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    ```

  - Remplacez l'url actuelle par celle de votre webhook

  - Ajouter le lancement du service automatiquement lors du démaragge de la VM avec la commande `systemctl enable fail2ban.service`

  - Puis le lancer avec `systemctl start fail2ban.service`

  - Vérifier qu'il est bien activé avec `systemctl status fail2ban.service`

Sur une troisième VM :

- Tester les configurations :

  - Récupérer une adresse IP du serveur :

    - Configurer l'interface en question et la mettre en dhcp

    - Relancer sa configuration en faisant `sudo ifdown <interface>` puis `sudo ifup <interface>`

    La requête DHCP se fera et une adresse IP nous sera fourni.

    ![](https://i.imgur.com/bg3KcaE.png)

  - Tester la configuration de portsentry :

    - Installer le paquet `nmap` avec les commandes `apt-get update`, `apt-get upgrade`, `apt-get install -y nmap`

    - Scanner la machine qui héberge le serveur apache avec la commande `nmap -sP <adresse_ip> `

    - Nmap n'arrive pas à scanner, ce qui veut dire que portsentry a bien bloqué le scan

      ![](https://i.imgur.com/hOz1boh.png)

    - Vérifier sur la machine apache si l'ip de la machine qui à lancé le scan nmap à bien été bloquée avec la commande `sudo iptables -L -n -v`

      ![](https://i.imgur.com/UskTqkt.png)

      On voit bien que l'ip a été bloqué

      Pour débloquer une adresse IP, aller dans le fichier `sudo vim /etc/hosts.deny` et supprimer la ligne qui bloque l'ip

  - Tester la configuration de fail2ban et des alertes discord :

    - Essayer de se connecter en ssh sur le serveur apache

    - Au bout de 5 tentatives, une alerte discord nous ping pour nous informer que tel adresse IP à tenté de se connecter et qu'elle à été bloqué.

      ![](https://i.imgur.com/y54T38q.png)

    - Vous pouvez vérifier que l'ip est bien bloqué avec la commande `sudo fail2ban-client status sshd`

Si vous voulez implémenter dans GNS3 :

- Créer un projet et importer vos VM dans Edit->Preferences

- Désactiver la NAT de vos VM

- Ajouter un switch et le relier aux VMs

- Ajouter un routeur, le relier au switch et lui mettre un adresse ip fixe

  - Entrer en mode conf `conf t`

  - éditer l'interface qui est reliée au switch `interface <interface>`

  - Lui mettre une adresse IP `ip address <ip> <masque>`

  - Allumer l'interface `no shut`

  - Puis sortir du mode conf avec `exit`

  - Votre routeur a maintenant un adresse IP sur cette interface

    ![](https://i.imgur.com/Za5hLKg.png)

- Ajouter le Cloud pour avoir accès à Internet et connectez le au routeur

- Côté routeur, mettre en dhcp sur l'interface qui est connecté à Internet, Suiver les mêmes procédures d'avant en changeant par `ip address dhcp`

- Il faut maintenant paramétrer la NAT :

  - Passer en mode conf `conf t`

  - Editer l'interface branché au switch avec ces commandes :

    - `interface <interface>`

    - `ip nat outside`

    - `exit`

  - Editer l'interface connecté à Internet avec ces commandes :

    - `interface <interface>`

    - `ip nat inside`

    - `exit`

  - Définir un traffic où tout est autorisé `access-list 1 permit any`

  - Configurer le NAT `ip nat inside source list 1 interface <interface_branché_switch> overload`

  - Vous avez maintenant un accès à Internet via le routeur

- Concernant les VMs, la connexion se fait par tunnel, il faudra donc reconfigurer les adresses IP et peut-être changer dans la configuration du serveur DHCP, l'interface en question dans le fichier `/etc/default/isc-dhcp-server`.
